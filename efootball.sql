-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2022 at 03:03 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `efootball`
--

-- --------------------------------------------------------

--
-- Table structure for table `pemain`
--

CREATE TABLE `pemain` (
  `id` int(6) NOT NULL,
  `user` int(6) NOT NULL,
  `kecepatan` int(3) NOT NULL,
  `ketepatan` int(3) NOT NULL,
  `kekuatan` int(3) NOT NULL,
  `kelincahan` int(3) NOT NULL,
  `keseimbangan` int(3) NOT NULL,
  `kicking` int(3) NOT NULL,
  `stoping` int(3) NOT NULL,
  `dribbling` int(3) NOT NULL,
  `heading` int(3) NOT NULL,
  `tackling` int(3) NOT NULL,
  `goal_keeping` int(3) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pemain`
--

INSERT INTO `pemain` (`id`, `user`, `kecepatan`, `ketepatan`, `kekuatan`, `kelincahan`, `keseimbangan`, `kicking`, `stoping`, `dribbling`, `heading`, `tackling`, `goal_keeping`, `created_at`) VALUES
(2, 25, 80, 79, 75, 78, 72, 81, 79, 76, 75, 82, 60, '2022-02-23'),
(3, 26, 80, 78, 76, 75, 79, 75, 69, 72, 73, 80, 60, '2022-02-23'),
(4, 27, 81, 79, 77, 75, 78, 80, 78, 79, 75, 78, 60, '2022-02-25'),
(5, 28, 79, 77, 77, 80, 76, 79, 81, 77, 76, 81, 60, '2022-02-25'),
(6, 29, 81, 79, 78, 79, 80, 76, 78, 79, 81, 80, 60, '2022-02-25'),
(7, 30, 80, 77, 79, 76, 75, 78, 76, 79, 75, 76, 60, '2022-02-25'),
(8, 31, 81, 80, 78, 79, 79, 80, 77, 80, 78, 77, 60, '2022-02-25'),
(9, 32, 80, 81, 80, 80, 79, 81, 78, 80, 77, 78, 60, '2022-02-25'),
(10, 33, 81, 80, 81, 81, 80, 79, 79, 80, 78, 80, 60, '2022-02-25'),
(11, 34, 78, 72, 75, 75, 74, 76, 75, 77, 75, 78, 60, '2022-02-25'),
(12, 35, 77, 78, 80, 75, 79, 78, 77, 80, 77, 78, 80, '2022-02-25'),
(13, 36, 79, 78, 80, 79, 77, 80, 78, 79, 77, 76, 60, '2022-02-25'),
(14, 37, 79, 80, 79, 81, 78, 80, 77, 79, 76, 76, 60, '2022-02-25'),
(15, 38, 78, 79, 81, 79, 77, 76, 80, 77, 81, 80, 60, '2022-02-25'),
(16, 39, 78, 79, 80, 77, 76, 77, 79, 76, 80, 80, 60, '2022-02-25'),
(17, 40, 70, 72, 71, 71, 73, 72, 73, 65, 75, 69, 78, '2022-02-25'),
(18, 41, 68, 74, 73, 70, 69, 74, 71, 73, 72, 69, 79, '2022-02-25');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `nomor_punggung` int(3) NOT NULL,
  `posisi` enum('Kiper','Back Kanan','Back Kiri','Center Back','Gelandang Kanan','Gelandang Tengah','Gelandang Kiri','Striker') NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `alamat`, `password`, `level`, `tanggal_lahir`, `nomor_punggung`, `posisi`, `created_at`) VALUES
(1, 'admin', 'Samsudin', 'admingudang@mail.com', '21232f297a57a5a743894a0e4a801fc3', 1, '0000-00-00', 0, 'Kiper', '2020-03-21'),
(25, 'deCNz', 'Arhan Pratama', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-23', 13, 'Back Kanan', '2022-02-23'),
(26, 'bambang', 'Bambang Pamungkas', 'Nusukan, Surakarta', 'd41d8cd98f00b204e9800998ecf8427e', 3, '2001-02-23', 10, 'Striker', '2022-02-23'),
(27, 'HQcvu', 'Bagas', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2003-02-25', 14, 'Striker', '2022-02-25'),
(28, 'BGdPb', 'Bagus', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2003-02-25', 15, 'Center Back', '2022-02-25'),
(29, 'gfQKH', 'Elkan', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 16, 'Back Kanan', '2022-02-25'),
(30, 'WYJKB', 'Kabuay', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 17, 'Gelandang Kiri', '2022-02-25'),
(31, 'OncMH', 'Witan', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 18, 'Gelandang Kanan', '2022-02-25'),
(32, 'DSrcm', 'Egy', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 19, 'Gelandang Tengah', '2022-02-25'),
(33, 'aGDkv', 'Asnawi', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 20, 'Back Kanan', '2022-02-25'),
(34, 'DLnCw', 'Edo', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 21, 'Kiper', '2022-02-25'),
(35, 'tjUYe', 'Dewangga', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 22, 'Kiper', '2022-02-25'),
(36, 'JvCiY', 'Rumakiek', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 23, 'Gelandang Kiri', '2022-02-25'),
(37, 'msuit', 'Abimanyu', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 24, 'Gelandang Kiri', '2022-02-25'),
(38, 'ubOdg', 'Irianto', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 25, 'Back Kanan', '2022-02-25'),
(39, 'Urzuq', 'Ryuji', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 26, 'Back Kiri', '2022-02-25'),
(40, 'VjEDT', 'Dedik', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 27, 'Kiper', '2022-02-25'),
(41, 'KgBxG', 'Yudo', 'Grogol', '5f4dcc3b5aa765d61d8327deb882cf99', 3, '2000-02-25', 28, 'Kiper', '2022-02-25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pemain`
--
ALTER TABLE `pemain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pemain`
--
ALTER TABLE `pemain`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pemain`
--
ALTER TABLE `pemain`
  ADD CONSTRAINT `pemain_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
