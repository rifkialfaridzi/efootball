# APLIKASI UPSELLING

# AKTOR
-> Admin        => Mengelola Sistem
-> Member       => Melakukan Pembelian dan Pembayaran (DISKON)
-> Non Member   => Melakukan Pembayaran dan Pembayaran (TIDAK DPT DISKON)

# HAK AKSES
->  Admin => 1
    + Login
    + Mengelola Stok Produk
    + Mengelola Member
    + Membuat User Baru (ADMIN LEV 2) => 2
    + Mengelola Penjualan
    + Mengelola Pembayaran
->  Member => 3
    + Registrasi
    + Recovery Password
    + Login
    + Melakukan Pembelian Produk -> Pembayaran
    + Kirim Bukti Pembayaran
    + DISKON
    + Data Pembelian
->  Non Member => 4
    + Melakukan Pembelian Produk -> Pembayaran
    + Kirim Bukti Pembayaran
    + Data Pembelian

# DATA MASTER
-> Data Category Produk
-> Data Stok Produk
-> Data Diskon
-> User (ADMIN LEV 2)

# DATABASE TABLE
-> Category [DONE]
-> Supplier [DONE]
-> Produk
-> Diskon
-> Penjualan
-> User

# FLOW 
->  Admin LEV 1
    + Membuat User (ADMIN LEV 2)
    + Input Category Produk
    + Stok Produk
    + Diskon
    + Validasi Bukti Pembayaran
        // Jika Valid, Transaksi Dapat Di Proses Ke Tahap Berikutnya -> Mengirimkan Notifikasi Ke WA
        // JIka Tidak Valid, Transaksi Tidak Dapat Di Proses -> Mengirimkan Notifikasi Ke WA
    + Cetak Laporan 
->  Member
    + Melakukan Registrasi
    + Melakukan Recovery Password
    + Melakukan Pembelian
    + Kirim Bukti Pembayaran
        // Jika Valid, Transaksi Dapat Di Proses Ke Tahap Berikutnya -> Mengirimkan Notifikasi Ke WA
        // JIka Tidak Valid, Transaksi Tidak Dapat Di Proses -> Mengirimkan Notifikasi Ke WA
    + Data Pembelian
-> Non Member
    + Melakukan Pembelian
    + Kirim Bukti Pembayaran
        // Jika Valid, Transaksi Dapat Di Proses Ke Tahap Berikutnya -> Mengirimkan Notifikasi Ke WA
        // JIka Tidak Valid, Transaksi Tidak Dapat Di Proses -> Mengirimkan Notifikasi Ke WA
    + Data Pembelian

# UPSELLING 
    => Menampilkan Rekomendasi Produk Sesuai Kategori 
    => Di Detail Produk
    => Kranjang Belanja

 Dengan cara membandingkan produk yang merk, kategorinya sama dan harga yang berada satu level diatas produk yang dipilih oleh customer, jadi apabila customer sudah memilih barang yang dia pilih maka  sistem  akan  segera  melakukan  analisis  kepada  barang  yang  dipilih  customer  karena  tidak semua barang masuk  dalam  kategori up selling,  apabila produk  yang dipilih masuk kedalam sistem up selling maka sistem akan segera melakukan penawaran produk. 2. Produk  yang  harganya  sudah  berada paling  atas  dan  tidak  ada  tandingan  harganya  dengan  produk satu merk dan satu kategori maka produk ini tidak masuk kedalam kriteria  up selling, karena sistem up selling  hanya  mengambil data harga  yang  satu  level diatasnya,  produk  yang di tawarkan  dalam sistem  up selling  ini  hanya  satu  produk  saja,  sedangkan  jika  produk  yang  memenuhi  kriteria  up selling lebih dari satu maka sistem akan mengambil produk pertama yang harganya satu level diatas dari produk yang dipilih customer, karena query pengurutan yang digunakan dalam sistem up selling ini adalah ascending (pengurutan dari atas kebawah)

https://api.whatsapp.com/send?phone=6281259929797&text=Nama%20%3A%0AAlamat%20%3A%0AUsia%20%3A%0AJenis%20kelamin%20%3A

