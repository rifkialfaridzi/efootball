<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model
{

    public $table = 'user';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        return $this->db->get($this->table)->result();
    }

    function user_admin_json() {
        $this->db->where('level',1);
        return $this->db->get($this->table)->result();
    }
    function user_member_json() {
        // $this->datatables->select('*');
        // $this->datatables->from('user');
        // //add this line for join
        // //$this->datatables->join('table2', 'user.field = table2.field');
        // $this->datatables->where('level',3);
        // return $this->datatables->generate();

        $this->db->where('level',3);
        return $this->db->get($this->table)->result();
    }

    function user_member() {
        $this->db->where('level', 3);
        return $this->db->get($this->table)->result();
    }

    function get_data_user_byid($id){

        $this->db->select('u.*,ud.id as id_detail, ud.alamat_penerima as alamat_penerima, ud.nama_penerima as nama_penerima');
        $this->db->from('user u'); 
        $this->db->join('user_detail ud', 'u.id=ud.user', 'left');
       
        return $this->db->where('u.id',$id);
    }
    
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_tlp($tlp)
    {
        $this->db->where('level', 3);
        $this->db->where('tlp', $tlp);
        return $this->db->get($this->table)->row();
    }
    function get_by_token_verifikasi($token_verifikasi)
    {
        $this->db->where('level', 3);
        $this->db->where('token_verifikasi', $token_verifikasi);
        return $this->db->get($this->table)->row();
    }

    function get_detail_member_byid($id_user)
    { 
        $this->db->select('ud.*,u.tlp as tlp');
        $this->db->from('user_detail ud'); 
        $this->db->join('user u', 'u.id=ud.user', 'left');
        $this->db->where('u.id',$id_user);
        return $this->db->where('u.level',3);
    }

    function get_detail_member_byid_kurir($id_user)
    { 
        $this->db->select('ud.*,u.tlp as tlp,k.alamat as alamat,k.harga as harga');
        $this->db->from('user_detail ud'); 
        $this->db->join('user u', 'u.id=ud.user', 'left');
        $this->db->join('kurir k', 'k.id=ud.kurir', 'left');
        $this->db->where('u.id',$id_user);
        return $this->db->where('u.level',3);
    }
   
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('email', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('image', $q);
	$this->db->or_like('level', $q);
	$this->db->or_like('created_at', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('email', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('image', $q);
	$this->db->or_like('level', $q);
	$this->db->or_like('created_at', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    function insert_user_detail($data)
    {
        $this->db->insert('user_detail', $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function update_by_userid($id, $data)
    {
        $this->db->where('user', $id);
        $this->db->update("user_detail", $data);
    }

    function update_user_detail($id, $data)
    {
        $this->db->where('user', $id);
        $this->db->update('user_detail', $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:24 */
/* http://harviacode.com */