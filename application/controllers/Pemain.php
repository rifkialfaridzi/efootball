<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pemain extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

	

		$this->load->model('Pemain_model');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function main(){
		
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in'))) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['dataPemain'] = $this->Pemain_model->get_by_idPemain($data_session['id'])->result();

		if (empty($data['dataPemain'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		}else{
		$data['main_content'] = 'pemain/detail';
		$data['page_title'] = 'Halaman Detail Pemain';
		$this->load->view('template', $data);
		}
	}

	public function index()
	{
		$data_session = $this->session->userdata;
		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$data['main_content'] = 'pemain/main';
		$data['page_title'] = 'Halaman Pemain';
        $this->load->view('template',$data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$pemain =  $this->Pemain_model->json();

		$data['draw'] = 0;
        $data['recordsTotal'] = $pemain == null ? [] : count($pemain);
        $data['recordsFiltered'] = $pemain == null ? [] : count($pemain);
        $data['data'] = $pemain == null ? [] : $pemain;
		
        echo json_encode($data);
	}


	public function create_action() 
	{
		//var_dump($this->input->post());
		$this->_rules_create();

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', 'Data Gagal Disimpan </br>'.validation_errors());
			redirect(site_url('pemain'));
			//echo validation_errors();
		} else {
			$data_pemain =  $this->input->post();
			$data_pemain['username'] = random_string('alpha', 5);
			$data_pemain['password'] = md5("password");
			$data_pemain['level'] = 3;
			$data_pemain['created_at'] = date("Y-m-d");
			
			// Insert Data Pemain
			$lastest_id = $this->Pemain_model->insert_data_pemain($data_pemain);
		
			$data_default_teknik_pemain = [
				"user" 			=> $lastest_id,
				"kecepatan" 	=> 0,
				"ketepatan" 	=> 0,
				"kekuatan" 		=> 0,
				"kelincahan" 	=> 0,
				"keseimbangan" 	=> 0,
				"kicking" 		=> 0,
				"stoping" 		=> 0,
				"dribbling" 	=> 0,
				"heading" 		=> 0,
				"tackling" 		=> 0,
				"goal_keeping" 	=> 0,
				"created_at" 	=> date("Y-m-d"),
			];
			// Insert Data Default Teknik Pemain
			$this->Pemain_model->insert_data_teknik_pemain($data_default_teknik_pemain);
			$this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
			redirect(site_url('pemain'));
		}
	}

	public function edit($id)
	{

		$row = $this->Pemain_model->get_by_id($id);

		if ($row) {
			$data = array(
				'data_profile' => $row,
				'data_pemain' => $this->Pemain_model->get_pemain_by_id($id),
				'main_content' => 'pemain/update',
                'page_title' => 'Edit Pemain'
			);
			$this->load->view('template', $data);
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('pemain'));
		}
	}
	
	public function update_akun($id)
	{
		
		$pemain = $this->Pemain_model->get_by_id($id);
		$is_username = $this->input->post('username', TRUE) != $pemain->username ? '|is_unique[user.username]' : '';

		$this->form_validation->set_rules('username', 'Username', 'required'.$is_username);//|edit_unique[barang.nama.' . $id . ']

		if ($this->form_validation->run() == FALSE) {
			 $this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>'.validation_errors());
            redirect(site_url('pemain'));
		} else {
			$data['username'] = $this->input->post('username', TRUE);
			$data['password'] = md5($this->input->post('password', TRUE));
			
			$this->Pemain_model->update_profile($id, $data);
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('pemain'));
		}
	}

	public function update_profile($id)
	{
		
		$pemain = $this->Pemain_model->get_by_id($id);
		$is_nomor_punggung = $this->input->post('nomor_punggung', TRUE) != $pemain->nomor_punggung ? '|is_unique[user.nomor_punggung]' : '';

		$this->form_validation->set_rules('nomor_punggung', 'Nomor Punggung', 'required'.$is_nomor_punggung);//|edit_unique[barang.nama.' . $id . ']

		if ($this->form_validation->run() == FALSE) {
			 $this->session->set_flashdata('pesan', 'Data Gagal Diubah </br>'.validation_errors());
            redirect(site_url('pemain'));
		} else {
			
			$this->Pemain_model->update_profile($id, $this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('pemain'));
		}
	}

	public function update_kriteria($id)
	{
		
		$pemain = $this->Pemain_model->get_by_id($id);
		$this->_rules_kriteria();

		if ($this->form_validation->run() == FALSE) {
			 $this->session->set_flashdata('pesan', 'Data Gagal Di Ubah');
            redirect(site_url('pemain'));
		} else {
			
			$this->Pemain_model->update($id, $this->input->post());
			$this->session->set_flashdata('pesan', 'Data Sukses Di Ubah');
			redirect(site_url('pemain'));
		}
	}

	public function delete($id)
	{
		$row = $this->Pemain_model->get_by_id($id);

		if ($row) {
			$this->Pemain_model->delete($id);
			$this->Pemain_model->delete_user($id);
			$this->session->set_flashdata('pesan', 'Data Berhasil Di Hapus');
			redirect(site_url('pemain'));
		} else {
			$this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
			redirect(site_url('pemain'));
		}
	}

	public function print_pemain($id=null) // CETAK LAPORAN 
    {
		$data_session = $this->session->userdata;

		if ($id == null) {
			$dataPemain = $this->Pemain_model->get_by_idPemain($data_session['id'])->result();
		}else{
			$dataPemain = $this->Pemain_model->get_by_idPemain($id)->result();
		}
        

        if ($dataPemain == null) {
            $this->session->set_flashdata('pesan', 'Data Tidak Ditemukan');
            redirect(site_url('lineup'));
        } else {
			//var_dump($dataPemain);
            $this->load->library('pdf');

            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->set_option('isRemoteEnabled', TRUE);
            $this->pdf->filename = "Laporan Pemain-" . $dataPemain[0]->name . "-" . $dataPemain[0]->posisi . ".pdf";
            $this->pdf->load_view('laporan/laporan_pemain', ['data' => $dataPemain]);
        }
    }

	public function _rules_kriteria()
	{
		$this->form_validation->set_rules('kecepatan', 'kecepatan', 'required');
		$this->form_validation->set_rules('ketepatan', 'ketepatan', 'required');
		$this->form_validation->set_rules('kekuatan', 'kekuatan', 'required');
		$this->form_validation->set_rules('kelincahan', 'kelincahan', 'required');
		$this->form_validation->set_rules('keseimbangan', 'keseimbangan', 'required');
		$this->form_validation->set_rules('stoping', 'stoping', 'required');
		$this->form_validation->set_rules('kicking', 'kicking', 'required');
		$this->form_validation->set_rules('dribbling', 'dribbling', 'required');
		$this->form_validation->set_rules('heading', 'heading', 'required');
		$this->form_validation->set_rules('tackling', 'tackling', 'required');
		$this->form_validation->set_rules('goal_keeping', 'goal_keeping', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function _rules_akun()
	{
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required|min_length[10]');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function _rules_create()
	{
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('nomor_punggung', 'Nomor Punggung', 'required|is_unique[user.nomor_punggung]');
		$this->form_validation->set_rules('posisi', 'Posisi', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-white">', '</span>');
	}

}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
