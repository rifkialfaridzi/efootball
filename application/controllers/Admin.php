<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $data_session = $this->session->userdata;

        if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
            redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
        }

     
        $this->load->model('Pemain_model');


        $this->load->model('auth_model');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        //var_dump($data_transaksi);
        $data['pemain'] = $this->Pemain_model->get_all_pemain();
        $data['main_content'] = 'admin/main';
        $data['page_title'] = 'Halaman Admin';
        $this->load->view('template', $data);
    }

    public function cek(){

        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth(6);
        var_dump($totalPendapatan);
    }

    public function dashboard($month)
    {

        $totalOrder =  count($this->Penjualan_model->total_order_bymonth($month));
        $totalProdukOrder = $this->Penjualan_model->jumlah_produk_order_bymonth($month)->total_produk;
        $totalPendapatan = $this->Penjualan_model->pendapatan_order_bymonth($month)->total_penjualan;
        $topProduk = $this->Penjualan_model->top_product_bymonth($month);
        $produk = $this->Barang_model->total_produk_bymonth($month);
        $supplier = $this->Supplier_model->total_supplier_bymonth($month);

        $data = [
            'totalOrder' => $totalOrder == null ? 0 : $totalOrder,
            'totalProdukOrder' => $totalProdukOrder == null ? 0 : $totalProdukOrder,
            'totalPendapatan' => $totalPendapatan == null ? 0 : $totalPendapatan,
            'topProduk' => $topProduk == null ? [] : $topProduk,
            'produk' => $produk,
            'supplier' => $supplier == null ? 0 : count($supplier),
        ];

        echo json_encode($data);
    }
}
