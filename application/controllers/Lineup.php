<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Lineup extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$data_session = $this->session->userdata;

		if ((!$this->session->userdata('logged_in')) || $data_session['level'] != 1 && $data_session['level'] != 2) {
			redirect('auth'); // Cek udah login apa belum, kalo belum login dulu
		}

		$this->load->model('Pemain_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index()
	{
		$data['main_content'] = 'lineup/main';
		$data['page_title'] = 'Halaman Lineup';
		$this->load->view('template', $data);
	}

	public function detail($id)
	{
		//var_dump($this->Pemain_model->get_by_idPemain($id)->result());	

		
		$data['dataPemain'] = $this->Pemain_model->get_by_idPemain($id)->result();

		if (empty($data['dataPemain'])) {
			$this->session->set_flashdata('pesan', 'Data Tidak DItemukan');
			redirect(site_url('lineup'));
		}else{
		$data['main_content'] = 'lineup/detail';
		$data['page_title'] = 'Halaman Lineup Detail';
		$this->load->view('template', $data);
		}



	}

	public function rekomendasi_by($posisi)
	{

		header('Content-Type: application/json');
		$pemain =  $this->Pemain_model->get_by_posisi(urldecode($posisi))->result_array();

		if (empty($pemain)) {
			$rekomendasi[] = [
				"name" => "",
				"nomor_punggung" => "",
				"posisi" => "",

				"teknik_fisik" => "",
				"teknik_dasar" => "",
				"points" => "",
				"rank" => ""
			];
		} else {
			foreach ($pemain as $key) {

				// Ekstract Data Atribut Pemain
				$dataKecepatan[] = $key['kecepatan'];
				$dataKetepatan[] = $key['ketepatan'];
				$dataKekuatan[] = $key['kekuatan'];
				$dataKelincahan[] = $key['kelincahan'];
				$dataKeseimbangan[] = $key['keseimbangan'];
				$dataKicking[] = $key['kicking'];
				$dataStoping[] = $key['stoping'];
				$dataDribling[] = $key['dribbling'];
				$dataHeading[] = $key['heading'];
				$dataTackling[] = $key['tackling'];
				$dataGoalKeeping[] = $key['goal_keeping'];



				// $normalisasiKecepatan []= round($key['kecepatan'] / max($dataKecepatan), 1);
				// $normalisasiKetepatan []= round($key['ketepatan'] / max($dataKetepatan), 1);
				// $normalisasiKekuatan []= round($key['kekuatan'] / max($dataKekuatan), 1);
				// $normalisasiKelincahan []= round($key['kelincahan'] / max($dataKelincahan), 1);
				// $normalisasiKeseimbangan []= round($key['keseimbangan'] / max($dataKeseimbangan), 1);
				// $normalisasiKicking []= round($key['kicking'] / max($dataKicking), 1);
				// $normalisasiStoping []= round($key['stoping'] / max($dataStoping), 1);
				// $normalisasiDribling []= round($key['dribling'] / max($dataDribling), 1);
				// $normalisasiHeading []= round($key['heading'] / max($dataHeading), 1);
				// $normalisasiTackling []= round($key['tackling'] / max($dataTackling), 1);
				// $normalisasiGoalKeeping []= round($key['goal_keeping'] / max($dataGoalKeeping), 1);


				$newRekomendasi[] = [
					"name" => $key['name'],
					"nomor_punggung" => $key['nomor_punggung'],
					"posisi" => $key['posisi'],

					"teknik" => $key,
					// "kecepatan" => round($key['kecepatan'] / max($dataKecepatan), 2),
					// "ketepatan" => round($key['ketepatan'] / max($dataKetepatan), 2),
					// "kekuatan" => round($key['kekuatan'] / max($dataKekuatan), 2),
					// "kelincahan" => round($key['kelincahan'] / max($dataKelincahan), 2),
					// "keseimbangan" => round($key['keseimbangan'] / max($dataKeseimbangan), 2),
					// "kicking" => round($key['kicking'] / max($dataKicking), 2),
					// "stoping" => round($key['stoping'] / max($dataStoping), 2),
					// "dribling" => round($key['dribbling'] / max($dataDribling), 2),
					// "heading" => round($key['heading'] / max($dataHeading), 2),
					// "tackling" => round($key['tackling'] / max($dataTackling), 2),
					// "goal_keeping" => round($key['goal_keeping'] / max($dataGoalKeeping), 2),

					"teknik_fisik" => round(
						round($key['kecepatan'] / max($dataKecepatan), 2) +
						round($key['ketepatan'] / max($dataKetepatan), 2) +
						round($key['kekuatan'] / max($dataKekuatan), 2) +
						round($key['kelincahan'] / max($dataKelincahan), 2) +
						round($key['keseimbangan'] / max($dataKeseimbangan), 2), 2),
					"teknik_dasar" => round(
						round($key['kicking'] / max($dataKicking), 2) +
						round($key['stoping'] / max($dataStoping), 2) +
						round($key['dribbling'] / max($dataDribling), 2) +
						round($key['heading'] / max($dataHeading), 2) +
						round($key['tackling'] / max($dataTackling), 2) +
						round($key['goal_keeping'] / max($dataGoalKeeping), 2), 2),
					"points" => round(
						round($key['kecepatan'] / max($dataKecepatan), 2) +
						round($key['ketepatan'] / max($dataKetepatan), 2) +
						round($key['kekuatan'] / max($dataKekuatan), 2) +
						round($key['kelincahan'] / max($dataKelincahan), 2) +
						round($key['keseimbangan'] / max($dataKeseimbangan), 2) +
						round($key['kicking'] / max($dataKicking), 2) +
						round($key['stoping'] / max($dataStoping), 2) +
						round($key['dribbling'] / max($dataDribling), 2) +
						round($key['heading'] / max($dataHeading), 2) +
						round($key['tackling'] / max($dataTackling), 2) +
						round($key['goal_keeping'] / max($dataGoalKeeping), 2), 2)

				];
			}
			$rekomendasi = [];
			$counter = 1;
			usort($newRekomendasi, function ($item1, $item2) { // SORT ARRAY BY POINT DES
				if ($item1['points'] == $item2['points']) return 0;
				return $item1['points'] < $item2['points'];
			});
			foreach ($newRekomendasi as $key) {
				$key['rank'] = $counter++;
				$rekomendasi[] = $key;
			}
		}


		//var_dump($rekomendasi);

		$data['draw'] = 0;
		$data['recordsTotal'] = $rekomendasi == null ? [] : count($rekomendasi);
		$data['recordsFiltered'] = $rekomendasi == null ? [] : count($rekomendasi);
		$data['data'] = $rekomendasi == null ? [] : $rekomendasi;

		echo json_encode($data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		$Lineup =  $this->Lineup_model->json();

		$data['draw'] = 0;
		$data['recordsTotal'] = $Lineup == null ? [] : count($Lineup);
		$data['recordsFiltered'] = $Lineup == null ? [] : count($Lineup);
		$data['data'] = $Lineup == null ? [] : $Lineup;

		echo json_encode($data);
	}
}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-21 14:22:20 */
/* http://harviacode.com */
