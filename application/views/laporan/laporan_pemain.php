<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

    <tbody>
        <tr>
            <td style="text-align:left;width:20%">
                <img style="width: auto;height:150px" src="<?php echo base_url('assets/img/logo.png') ?>"></img>
            </td>
            <td style="text-align:center">
                <h1 style="text-align:center;padding:0;margin:0"><strong>SSB SOLO BARAT FC</strong></h1>
                <small>Jl Kampung Baru No 2 RT 03 RW07 Pabelan, Kartasura, Sukoharjo</small>
                <h4>Laporan Pemain</h4>
            </td>
        </tr>
    </tbody>

</table>

<hr>

<div style="text-align:center">

    <p>&nbsp;</p>
    <h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">Data Pemain</h4>
    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
        <tbody>
            <tr>

                <td style="text-align:left; padding: 2px 5px 2px 5px">Nama</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Nomor Punggung</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Posisi</td>
            </tr>

            <?php foreach ($data as $key) { ?>
                <tr>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->name; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->nomor_punggung; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->posisi; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">Teknik Fisik</h4>
    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin: 1px auto; padding:10px; width:100%">

        <tbody>
            <tr>

                <td style="text-align:left; padding: 2px 5px 2px 5px">No</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Kriteria</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Score</td>

            </tr>
            <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px">1</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Kecepatan</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->kecepatan; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">2</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Ketepatan</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->ketepatan; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">3</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">kekuatan</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->kekuatan; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">4</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Kelincahan</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->kelincahan; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">5</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Keseimbangan</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->keseimbangan; ?></td>
            </tr>
        </tbody>
    </table>

    <h4 style="text-align: left;margin-bottom: -0.3em;margin-left: 0.5em;">Teknik Dasar</h4>
    <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin: 1px auto; padding:10px; width:100%">

        <tbody>
            <tr>

                <td style="text-align:left; padding: 2px 5px 2px 5px">No</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Kriteria</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Score</td>

            </tr>
            <tr>
            <td style="text-align:left; padding: 2px 5px 2px 5px">1</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Kicking</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->kicking; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">2</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Stoping</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->stoping; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">3</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Dribbling</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->dribbling; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">4</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Heading</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->heading; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">5</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Tackling</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->tackling; ?></td>
            </tr>
            <tr>
                <td style="text-align:left; padding: 2px 5px 2px 5px">6</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px">Goal Keeping</td>
                <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $data[0]->goal_keeping; ?></td>
            </tr>
        </tbody>
    </table>

    <p>&nbsp;</p>

    <p>&nbsp;</p>

    <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
        <tbody>
            <tr>
                <td>Yang Mengetahui,</td>
            </tr>
            <tr>
                <td><span style="font-size:16px"><strong>Badaruddin</strong></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Pimpinan PS LDII SOBAR</td>
            </tr>
        </tbody>
    </table>
</div>