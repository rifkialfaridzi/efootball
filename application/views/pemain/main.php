<section class="section">
	<div class="section-header">
		<h1>Halaman Pemain</h1>
	</div>

	<div class="section-body">
		<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan');?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-lg-4 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Tambah Pemain</h4>
					</div>
					<div class="collapse show" id="mycard-collapse">
						<div class="card-body">
							<form method="POST" action="<?php echo base_url("pemain/create_action"); ?>" class="needs-validation" novalidate="">
								<div class="form-group">
									<label for="name">Nama Pemain</label>
									<input id="name" type="text" class="form-control" name="name" tabindex="1" placeholder="Bambang" required autofocus>
									<div class="invalid-feedback">
										Nama Pemain Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Tanggal Lahir</label>
									<input id="tanggal_lahir" type="text" class="form-control datepicker" name="tanggal_lahir" tabindex="1" required autofocus>
									<div class="invalid-feedback">
										Tanggal Lahir Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label for="nama">Nomor Punggung</label>
									<input id="nomor_punggung" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" class="form-control" name="nomor_punggung" tabindex="1" placeholder="10" required autofocus>
									<div class="invalid-feedback">
										Nomor Punggung Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<label>Pilih Posisi Pemain</label>
									<select name="posisi" class="form-control select2">
										<option value="">Pilih Posisi</option>
										<option value="Kiper">Kiper</option>
										<option value="Back Kanan">Back Kanan</option>
										<option value="Back Kiri">Back Kiri</option>
										<option value="Center Back">Center Back</option>
										<option value="Gelandang Kanan">Gelandang Kanan</option>
										<option value="Gelandang Tengah">Gelandang Tengah</option>
										<option value="Gelandang Kiri">Gelandang Kiri</option>
										<option value="Striker">Striker</option>
									</select>
								</div>
								<div class="form-group">
									<label for="nama">Alamat</label>
									<textarea id="alamat" type="text" class="form-control" name="alamat" tabindex="1" rows="4" cols="50" required autofocus></textarea>
									<div class="invalid-feedback">
										Alamat Masih Kosong
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-success btn-lg btn-block" tabindex="4">
										Tambah
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Data Pemain</h4>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="category_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>Nama Pemain</th>
										<th>Posisi</th>
										<th>Nomor Punggung</th>
										<th>Tanggal Masuk</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="exampleModalDelete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Pemain</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah Anda Yakin ?</p>
			</div>
			<div class="modal-footer bg-whitesmoke br">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
				<a id="btn-delete" type="button" href="#" class="btn btn-danger">Hapus</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {
		//datatables
		table = $('#category_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pemain/json'); ?>',
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columns": [{
					"data": "name"
				},
				{
					"data": "posisi"
				},
				{
					"data": "nomor_punggung"
				},
				{
					"data": "created_at"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("pemain/edit/") ?>' + row.id + '" class="btn btn-icon btn-primary"><i class="far fa-edit"></i></a> <a onclick=deleteConfirm("' + row.id + '") href="#!" class="btn btn-icon btn-danger exampleModalDelete" data-toggle="modal" data-target="#exampleModalDelete"><i class="fas fa-times"></i></a>';
					}
				}
			],

		});



	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("pemain/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}
</script>