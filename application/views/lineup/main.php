<section class="section">
	<div class="section-header">
		<h1>Halaman Line Up</h1>
	</div>

	<div class="section-body">
	<?php if ($this->session->flashdata('pesan')) { ?>
			<div class="alert alert-warning alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					<?php echo $this->session->flashdata('pesan');?>
				</div>
			</div>
		<?php } ?>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Statistik Pemain</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Pilih Posisi Pemain</label>
							<select id="posisi_selector" name="posisi" class="form-control select2">
								<option value="">Pilih Posisi</option>
								<option value="Kiper" selected>Kiper</option>
								<option value="Back Kanan">Back Kanan</option>
								<option value="Back Kiri">Back Kiri</option>
								<option value="Center Back">Center Back</option>
								<option value="Gelandang Kanan">Gelandang Kanan</option>
								<option value="Gelandang Tengah">Gelandang Tengah</option>
								<option value="Gelandang Kiri">Gelandang Kiri</option>
								<option value="Striker">Striker</option>
							</select>
						</div>
						<div class="table-responsive">
							<table id="unit_tabel" class="table table-striped">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Nomor Punggung</th>
										<th>Posisi</th>
										<th>Teknik Fisik</th>
										<th>Teknik Dasar</th>
										<th>Point</th>
										<th>Rank</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	$("#posisi_selector").change(function() {

		table.ajax.url("<?php echo site_url('lineup/rekomendasi_by/'); ?>" + $("#posisi_selector").val()).load();
	});

	var save_method; //for save method string
	var table;

	var date = new Date();
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

	var firstdatestring = firstDay.getFullYear() + "-" + (("0" + (firstDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + firstDay.getDate()).slice(-2);
	var lastdatestring = lastDay.getFullYear() + "-" + (("0" + (lastDay.getMonth() + 1)).slice(-2)) + "-" + ("0" + lastDay.getDate()).slice(-2);

	console.log(firstdatestring);
	console.log(lastdatestring);
	$(document).ready(function() {
		//datatables
		conter = 0;
		table = $('#unit_tabel').DataTable({
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('lineup/rekomendasi_by/kiper'); ?>',
				"type": "POST"
			},
			"order": [
				[6, "asc"]
			],
			"bFilter": false,
			//Set column definition initialisation properties.
			"columns": [{
					"data": "name"
				},
				{
					"data": "nomor_punggung"
				},
				{
					"data": "posisi"
				},
				{
					"data": "teknik_fisik"
				},
				{
					"data": "teknik_dasar"
				},
				{
					"data": null,
					"render": function(data, type, row) {
						return row.points +'/11';
					}
				},
				{
					"data": "rank"
				},

				{
					"data": null,
					"render": function(data, type, row) {
						return '<a href="<?php echo site_url("lineup/detail/") ?>'+row.teknik.id_pemain+'" class="btn btn-icon btn-primary"><i class="far fa-eye"></i></a>';
					}
				}
			],

		});

		var getFirstDate = firstdatestring;
		var getLastDate = lastdatestring;

		$("#cetak").click(function() {
			window.open("<?php echo site_url('penjualan/print_penjualan/?'); ?>start=" + getFirstDate + "&end=" + getLastDate + "&status=" + $("#select_status").val());
			// console.log(getFirstDate);
			// console.log(getLastDate);
		});

		$('.daterange-cus').daterangepicker({

				locale: {
					format: 'YYYY-MM-DD'
				},
				drops: 'down',
				opens: 'right',
			},
			function(start, end) {
				console.log("Callback has been called!" + start.format('YYYY-MM-DD') + " to " + end.format('YYYY-MM-DD'));

				table.ajax.url("<?php echo site_url('penjualan/penjualan_all_json/?'); ?>start=" + start.format('YYYY-MM-DD') + "&end=" + end.format('YYYY-MM-DD') + "&status=" + $("#select_status").val()).load();

				getFirstDate = start.format('YYYY-MM-DD');
				getLastDate = end.format('YYYY-MM-DD');
				$("#cetak").removeClass('disabled');

			}).val(firstdatestring + "-" + lastdatestring);


		// Daterangepicker
		if (jQuery().daterangepicker) {
			if ($(".datepicker").length) {
				$('.datepicker').daterangepicker({
					locale: {
						format: 'YYYY-MM-DD'
					},
					singleDatePicker: true,
				});
			}
			if ($(".datetimepicker").length) {
				$('.datetimepicker').daterangepicker({
					locale: {
						format: 'YYYY-MM-DD hh:mm'
					},
					singleDatePicker: true,
					timePicker: true,
					timePicker24Hour: true,
				});
			}
			if ($(".daterange").length) {
				$('.daterange').daterangepicker({
					locale: {
						format: 'YYYY-MM-DD'
					},
					drops: 'down',
					opens: 'right'
				});
			}
		}

	});

	function deleteConfirm(url) {
		$('#btn-delete').attr('href', "<?php echo site_url("barang/delete"); ?>/" + url);
		$('#exampleModalDelete').modal();
	}




	$.uploadPreview({
		input_field: "#image-upload", // Default: .image-upload
		preview_box: "#image-preview", // Default: .image-preview
		label_field: "#image-label", // Default: .image-label
		label_default: "Choose File", // Default: Choose File
		label_selected: "Change File", // Default: Change File
		no_label: false, // Default: false
		success_callback: null // Default: null
	});
</script>