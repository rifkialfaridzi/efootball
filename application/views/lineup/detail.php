<section class="section">
	<div class="section-header">
		<h1>Statistik Pemain : <?php echo $dataPemain[0]->name; ?></h1>
	</div>

	<div class="section-body">
		<div class="row">
			<div class="col-lg-6 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Rating Pemain</h4>
						<div class="card-header-action">
							<a class="btn btn-icon btn-info" href="<?php echo base_url('pemain/print_pemain/'.$dataPemain[0]->id_pemain) ?>"><i class="fas fa-print"></i></a>
						</div>
					</div>
					<div class="card-body">
						<table id="unit_tabel" class="table table-striped">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Nomor Punggung</th>
									<th>Posisi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $dataPemain[0]->name ?></td>
									<td><?php echo $dataPemain[0]->nomor_punggung ?></td>
									<td><?php echo $dataPemain[0]->posisi ?></td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="card-body">
						<table id="unit_tabel" class="table table-striped">
							<thead>
								<tr>
									<th>Kriteria</th>
									<th>Skor</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Kecepatan</td>
									<td><?php echo $dataPemain[0]->kecepatan; ?></td>
								</tr>
								<tr>
									<td>Ketepatan</td>
									<td><?php echo $dataPemain[0]->ketepatan; ?></td>
								</tr>
								<tr>
									<td>Kekuatan</td>
									<td><?php echo $dataPemain[0]->kekuatan; ?></td>
								</tr>
								<tr>
									<td>Kelincahan</td>
									<td><?php echo $dataPemain[0]->kelincahan; ?></td>
								</tr>
								<tr>
									<td>Keseimbangan</td>
									<td><?php echo $dataPemain[0]->keseimbangan; ?></td>
								</tr>
								<tr>
									<td>Kicking</td>
									<td><?php echo $dataPemain[0]->kicking; ?></td>
								</tr>
								<tr>
									<td>Stoping</td>
									<td><?php echo $dataPemain[0]->stoping; ?></td>
								</tr>
								<tr>
									<td>Dribbling</td>
									<td><?php echo $dataPemain[0]->dribbling; ?></td>
								</tr>
								<tr>
									<td>Heading</td>
									<td><?php echo $dataPemain[0]->heading; ?></td>
								</tr>
								<tr>
									<td>Tackling</td>
									<td><?php echo $dataPemain[0]->tackling; ?></td>
								</tr>
								<tr>
									<td>Goal Keeping</td>
									<td><?php echo $dataPemain[0]->goal_keeping; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 col-12 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h4>Statistik Pemain</h4>
						<div class="card-header-action">
							<a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-minus"></i></a>
						</div>
					</div>
					<div class="card-body">
						<canvas id="myChart" width="400" height="400"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<script type="text/javascript">
	var save_method; //for save method string
	var table;

	const ctx = document.getElementById('myChart').getContext('2d');
	const data = {
		labels: [
			'Kecepatan',
			'Ketepatan',
			'Kekuatan',
			'Kelincahan',
			'Keseimbangan',
			'Kicking',
			'Stoping',
			'Dribbling',
			'Heading',
			'Tackling',
			'Goal Keeping'
		],
		datasets: [{
			label: 'Statistik Pemain',
			data: [
				<?php echo $dataPemain[0]->kecepatan ?>, 
				<?php echo $dataPemain[0]->ketepatan ?>,
				<?php echo $dataPemain[0]->kekuatan ?>,
				<?php echo $dataPemain[0]->kelincahan ?>,
				<?php echo $dataPemain[0]->keseimbangan ?>,
				<?php echo $dataPemain[0]->kicking ?>,
				<?php echo $dataPemain[0]->stoping ?>,
				<?php echo $dataPemain[0]->dribbling ?>,
				<?php echo $dataPemain[0]->heading ?>,
				<?php echo $dataPemain[0]->tackling ?>,
				<?php echo $dataPemain[0]->goal_keeping ?>],
			fill: true,
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgb(255, 99, 132)',
			pointBackgroundColor: 'rgb(255, 99, 132)',
			pointBorderColor: '#fff',
			pointHoverBackgroundColor: '#fff',
			pointHoverBorderColor: 'rgb(255, 99, 132)'
		}]
	};
	const myChart = new Chart(ctx, {
		type: 'radar',
		data: data,
		options: {
			scales: {
				y: {
					beginAtZero: true
				}
			}
		}
	});
</script>